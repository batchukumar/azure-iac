#!/bin/bash

# Download Ubuntu Server ISO (example URL, replace with the actual URL)
UBUNTU_ISO_URL="https://releases.ubuntu.com/20.04.3/ubuntu-20.04.3-live-server-amd64.iso"

# Define VM name
VM_NAME="my-vm"

# Create VM
VBoxManage createvm --name $VM_NAME --register

# Configure VM settings
VBoxManage modifyvm $VM_NAME --memory 2048 --cpus 2 --ostype Ubuntu_64 --nic1 nat
VBoxManage storagectl $VM_NAME --name "SATA Controller" --add sata

# Attach ISO to VM
VBoxManage storageattach $VM_NAME --storagectl "SATA Controller" --port 0 --device 0 --type dvddrive --medium $UBUNTU_ISO_URL

# Start the VM
VBoxManage startvm $VM_NAME --type headless

# Wait for VM to boot (adjust sleep duration as needed)
sleep 60

# Perform further configuration or provisioning here

