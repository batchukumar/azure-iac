provider "virtualbox" {
  version = "~> 2.0"
}

resource "virtualbox_vm" "my_vm" {
  name           = "my-vm"
  cpus           = 2
  memory         = "2048"
  network_adapter {
    host_interface_type = "VirtualBox Host-Only Ethernet Adapter"
  }
  storage_controller {
    name = "SATA Controller"
    bus  = "0"
  }
}

resource "null_resource" "provision_vm" {
  triggers = {
    virtualbox_vm_id = virtualbox_vm.my_vm.id
  }

  provisioner "local-exec" {
    command = "scripts/setup_vm.sh"
  }
}

